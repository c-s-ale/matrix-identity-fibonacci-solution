The standard approach to the recursive solution has time complexity $`O(n)`$, with the required memoization adding a space complexity of $`O(n)`$.

Using identities derived through the [matrix form](https://en.wikipedia.org/wiki/Fibonacci_number#Matrix_form), we can produce a faster algorithm that scales much better!

Here are the required identities:

$` F_{2n-1} = {F_n} ^2 + {F_{n-1}}^2 `$

$` F_{2n} = (2F_{n - 1} + F_n)F_n `$


The new time complexity for this algorithm is $`O(M(n)log(n))`$ where $`M(n)`$ is the time complexity for multiplying two $`n`$-digit numbers together. The space complexity is still up to $`O(n)`$ due to the memoization. 

In addition, this optimized solution avoids running into the recursion depth in Python nearly as quickly. Where the normal solution can't even calculate up to $`n`$ = 1,000, the optimized variation can compute well past $`n`$ = 1,000,000 without running into the default recursion depth in Python.
