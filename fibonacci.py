def fib(n, memo = {0: 0, 1: 1}):
    if n in memo:
        return memo[n]
    
    if (n & 1):
        k = (n + 1) // 2
    else:
        k = n // 2

    if (n & 1):
        # identity for odd k
        memo[n] = fib(k, memo)**2 + fib(k - 1, memo)**2
    else:
        # identity for even k
        memo[n] = (2 * fib(k - 1, memo) + fib(k, memo)) * fib(k, memo)

    return memo[n]
    

if __name__ == '__main__':
    n = int(input('Enter a number: '))
    print(f"The nth Fibonacci number at n = {n} is:", fib(n))